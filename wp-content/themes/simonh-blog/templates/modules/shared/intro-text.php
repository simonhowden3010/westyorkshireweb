<?php 
	$intro_text_title = get_field('intro_text_title');
	$intro_text_main = get_field('intro_text_main_text');
	$intro_text_sidebar_switch = get_field('intro_text_sidebar_switch');
	$intro_sidebar_title = get_field('sidebar_title');
	$intro_sidebar_text = get_field('sidebar_text');

	if($intro_text_main): ?>
		<section class="intro-text row">
			<div class="intro-text-inner">
				<article class="col8">
					<header><?php echo $intro_text_title; ?></header>
					<?php if($intro_text_main): ?><p><?php echo $intro_text_main; ?></p><?php endif; ?>
					<span class="effects">&nbsp;</span>
				</article>
				<?php if($intro_text_sidebar_switch == 'Yes'): ?>
					<aside class="col4">
						<header><?php echo $intro_sidebar_title; ?></header>
						<?php if($intro_sidebar_text): ?><p><?php echo $intro_sidebar_text; ?></p><?php endif; ?>
						<span class="effects">&nbsp;</span>
					</aside>
				<?php endif; ?>
			</div>
		</section>
	<?php endif;
?>

<section class="intro-text row">
	<div class="intro-text-inner">
		<article class="col8">
			<header>My intro text title</header>
			<p>Maybe there was an old trapper that lived out here and maybe one day he went to check his beaver traps, and maybe he fell into the river and drowned. And just raise cain. Remember how free clouds are. They just lay around in the sky all day long.</p>
			<span class="effects">&nbsp;</span>
		</article>
		<aside class="col4">
			<header>My sidebar title goes here</header>
			<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet </p>
			<span class="effects">&nbsp;</span>
		</aside>
	</div>
</section>