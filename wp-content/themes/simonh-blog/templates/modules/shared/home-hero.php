<section class="hero-outer">
	<div class="container">
		<div class="row">
			<div class="col12 hero-inner">
					<?php  
					//hero_title = get_field('hero_title');
					//$hero_subtitle = get_field('hero_subtitle');

					//if($hero_title): ?>
						<!-- <h1><?php echo $hero_title; ?></h1> -->
					<?php //endif; 

					//if($hero_subtitle): ?>
						<!-- <h2><?php echo $hero_subtitle; ?></h2> -->
					<?php //endif; ?>


					<h1>My hero title goes here</h1>
					<h2>That's the way I look when I get home late; black and blue. The secret to doing anything is believing that you can do it.</h2>
			</div>
		</div>
	</div>
</section>