<section class="three-post-listing row">
	<div class="col12">
		<h2 class="three-post-listing__title">Latest Posts</h2>
	</div>
	<div class="three-post-listing-inner">
		<a href="#" title="My Blog Post title" class="col4">
			<h2>My Blog Post title</h2>
		    <img src="http://via.placeholder.com/640x360" alt="" />
			<p>My blog post snippet My blog post snippet My blog post snippet My blog post snippet...</p>
		</a href="#" title="My Blog Post title">
		<a href="#" title="My Blog Post title" class="col4">
			<h2>My Blog Post title</h2>
		    <img src="http://via.placeholder.com/640x360" alt="" />
			<p>My blog post snippet My blog post snippet My blog post snippet My blog post snippet...</p>
		</a href="#" title="My Blog Post title">
		<a href="#" title="My Blog Post title" class="col4">
			<h2>My Blog Post title</h2>
		    <img src="http://via.placeholder.com/640x360" alt="" />
			<p>My blog post snippet My blog post snippet My blog post snippet My blog post snippet...</p>
		</a href="#" title="My Blog Post title">
	</div>
</section>