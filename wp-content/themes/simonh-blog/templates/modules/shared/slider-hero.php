<div class="hero-outer row">
	<div class="container">
		<div class="hero-inner site-wide clearfix">
			<section class="col-desk-12 col-tab-12 col-mob-12">
				<h1>WestYorkshireWeb</h1>
				<h2>One lad, many hobbies.</h2>
				<p class="hero-quote commadore">Web, Tech, Music, Chip</p>
			</section>
		</div>
	</div>
</div>